import numpy as np
import cv2

class Tracker():
    def __init__(self, card=0):
        self.cap = cv2.VideoCapture(card)

    def find(self):
        # Capture frame-by-frame
        ret, frame = self.cap.read()

        bluegreen_subtraction = np.asarray(frame).astype(int)
        bluegreen_subtraction = bluegreen_subtraction[:,:,2] - bluegreen_subtraction[:,:,0]

        # make negative numbers 0
        bluegreen_subtraction = bluegreen_subtraction.clip(min=0)
        
        # threshold
        ret, thresh = cv2.threshold(np.uint8(bluegreen_subtraction),60,255,cv2.THRESH_BINARY)
        
        # invert
        # thresh = (thresh * -1) + 256

        img = cv2.medianBlur(thresh, 5)

        img = np.uint8(img)
        circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,60,
                                param1=100,param2=16,minRadius=100,maxRadius=400)

        circlepic = np.zeros(frame.shape)

        if (circles is not None):

            circles = np.uint16(np.around(circles))
            for i in circles[0,:]:

                # draw the outer circle
                cv2.circle(circlepic,(i[0],i[1]),i[2],(0,255,0),2)
                # draw the center of the circle
                cv2.circle(circlepic,(i[0],i[1]),2,(0,0,255),3)

        # Display the resulting frame
        cv2.imshow('Image', frame)
        cv2.imshow('Red Circles', circlepic)
        cv2.waitKey(1)

t = Tracker()
while True:
    t.find()